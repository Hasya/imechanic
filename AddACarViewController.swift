//
//  CarViewController.swift
//  IOS_P
//
//  Created by Sandeep Velaga on 3/27/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit
import Parse

class AddACarViewController: UIViewController {
    
    var vinnumber:Int = 0
    var carobjects:[Car] = []
    
    @IBOutlet weak var vinTF: UITextField!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
}
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

    if (PFUser.current() == nil) {
    DispatchQueue.main.async(execute: { () -> Void in
    
    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login")
    self.present(viewController, animated: true, completion: nil)
    })
    } else {
    fetchData()
    }
}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Actions
    
    @IBAction func Logout(_ sender: UIButton) {
        PFUser.logOut()

        DispatchQueue.main.async(execute: { () -> Void in
            let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login")
            self.present(viewController, animated: true, completion: nil)
        })

    }
    
    
    
    func fetchData(){
      let query = PFQuery(className:"Car")
      query.whereKey("userID", equalTo: (PFUser.current()?.username!)!)
//      query.findObjectsInBackgroundWithBlock { as!
//           (objects: [PFObject]?, error: NSError?) -> Void in
//            if error == nil {
//               self.carobjects = objects as! [Car]
//           } else {
//                self.vinnumber = self.vin
//
//           }
//        }
    }

}

//prepare for segue to detailsvc for car name




