//
//  CarDatabase.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 3/28/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import Foundation
import Parse

class CarDatabase:PFObject,PFSubclassing {
    @NSManaged var vin:Int
    @NSManaged var carName:String

    static func parseClassName() -> String {
    return "CarDatabase"
    }
}

