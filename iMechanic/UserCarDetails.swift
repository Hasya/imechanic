//
//  UserCarDetails.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 4/2/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import Foundation
import Parse

class UserCarDetails:PFObject,PFSubclassing {
    @NSManaged var vin:Int
    @NSManaged var carName:String
    @NSManaged var userID:String
    @NSManaged var registrationDate:String
    @NSManaged var tireRotationDate:String
    @NSManaged var oilChangeDate:String
    @NSManaged var safetyInspectionDate:String
    @NSManaged var mileage:Int
    
    static func parseClassName() -> String {
        return "UserCarDetails"
    }
}
