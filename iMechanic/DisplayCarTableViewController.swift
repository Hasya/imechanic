//
//  DisplayCarTableViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 4/1/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit
import Parse

class DisplayCarTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
   // var carDetails:UserCarDetails = UserCarDetails()
    var carObject:[CarDatabase] = []

    var userCars:[UserCarDetails] = []
    var selectedCar:UserCarDetails!
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (PFUser.current() == nil) {
            DispatchQueue.main.async(execute: { () -> Void in
                
                let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login")
                self.present(viewController, animated: true, completion: nil)
            })
        } else {
            self.tableView.reloadData()
            fetchData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func logOut(_ sender: UIBarButtonItem) {
        PFUser.logOut()
        
        DispatchQueue.main.async(execute: { () -> Void in
            let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login")
            self.present(viewController, animated: true, completion: nil)
        })
    }

    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userCars.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "carCell", for: indexPath)
        
        cell.textLabel!.text = userCars[indexPath.row].carName

        return cell
    }

 //Override to support conditional editing of the table view.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCar = self.userCars[(tableView.indexPathForSelectedRow?.row)!]
        self.performSegue(withIdentifier: "carDetailsSegue", sender: nil)

    }
    
    // MARK: FetchData()
    
    func fetchData(){
        let query = PFQuery(className:"UserCarDetails")
        query.whereKey("userID", equalTo: (PFUser.current()?.username!)!)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                self.userCars = objects as! [UserCarDetails]
                
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "carDetailsSegue" {
            let carDetailsTabC = segue.destination as! DashboardTabBarController
            
//            let nav = carDetailsTabC.viewControllers?[0] as! UINavigationController
//            
//            let dashboard = nav.viewControllers[0] as! DashboardTableViewController
//            
//            print("Inside prepareForSegue of VINViewThingy")
//            
//            dashboard.carDetails.carName = selectedCar.carName
//            print(dashboard.carDetails.carName)
            
            carDetailsTabC.carDetails = selectedCar
            carDetailsTabC.carDetails = self.userCars[(self.tableView.indexPathForSelectedRow?.row)!]
            print(selectedCar.carName)
        }
    }
    
     // Override to support editing the table view.
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
        
         self.userCars.first?.deleteInBackground(block:{ (success, error) -> Void in
            print("The car has been deleted from the user's list.")
            self.tableView.reloadData()
            self.carObject.first?.saveInBackground(block:{ (success, error) -> Void in
                print("The car has been added to the database.")
                self.navigationController?.popViewController(animated: true)
            } )

           self.tableView.reloadData()
            
        } )
//        self.carObject.first?.saveInBackground(block:{ (success, error) -> Void in
//            print("The car has been added to the user's list.")
//            self.navigationController?.popViewController(animated: true)
//        } )



     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
        self.tableView.reloadData()
     } else if editingStyle == .insert {
       
//        self.userCars.first?.remove(forKey: "registrationDate")
//        self.userCars.first?.remove(forKey: "tireRotationDate")
//        self.userCars.first?.remove(forKey: "oilChangeDate")
//        self.userCars.first?.remove(forKey: "safetyInspectionDate")
//
//        self.carObject.first?.saveInBackground(block:{ (success, error) -> Void in
//            print("The car has been added to the user's list.")
//            self.navigationController?.popViewController(animated: true)
//        } )
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
    
 

}
