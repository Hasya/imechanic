//
//  ForgotPasswordViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 3/5/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit
import Parse

class ForgotPasswordViewController: UIViewController {
    
        @IBOutlet var emailTF: UITextField!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            
            view.addGestureRecognizer(tap)
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        // Method to reset the user's password
        
        @IBAction func resetPassword(_ sender: AnyObject) {
            let email = self.emailTF.text
            let finalEmail = email!.trimmingCharacters(in:.whitespaces)
            
            // Send a request to reset a password
            
            PFUser.requestPasswordResetForEmail(inBackground: finalEmail)
            
            let alert = UIAlertController (title: "Password Reset", message: "An email containing information on how to reset your password has been sent to " + finalEmail + ".", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        // Methods to dismiss the keyboard
        
        func dismissKeyboard() {
            view.endEditing(true)
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
            return false
        }
        
        
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
         // Get the new view controller using segue.destinationViewController.
         // Pass the selected object to the new view controller.
         }
         */
        
}
