//
//  CarViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 3/27/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit
import Parse

class AddACarViewController: UIViewController {
    
    var carDetails:UserCarDetails = UserCarDetails()
    var carObject:[CarDatabase] = []
    var carVIN:Int!
    
    @IBOutlet weak var vinTF: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func AddACar(_ sender: UIButton) {
        let query = PFQuery(className:"CarDatabase")
        query.whereKey("vin", equalTo: (Int(vinTF.text!))!)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                self.carObject = objects as! [CarDatabase]
                print("In AddACar:\n\(self.carObject)")
                for object in self.carObject {
                    self.carDetails.carName = object.carName
                    self.carDetails.vin = object.vin
                    self.carDetails.userID = (PFUser.current()?.username!)!
                }
//                self.carDetails.carName = (self.carObject.first?.carName)!
//                self.carDetails.vin = (self.carObject.first?.vin)!
                
                self.carDetails.saveInBackground(block: { (success, error) -> Void in
                    print("The car has been added to the user's list.")
                    //Add an alert with the same message.
                    self.carObject.first?.deleteInBackground(block:{ (success, error) -> Void in
                        print("The car has been deleted from the database")
                        self.navigationController?.popViewController(animated: true)
                        
                    } )
                 
                    
                })
//                self.carObject.first?.deleteInBackground(block:{ (success, error) -> Void in
//                    print("The car has been added to the user's list.")
//                    self.navigationController?.popViewController(animated: true)
//
//                } )
            }
        }
        
    }
    
//    func fetchAndStoreData(){
//        let query = PFQuery(className:"CarDatabase")
//        query.whereKey("vin", equalTo: (Int(vinTF.text!))!)
//        query.findObjectsInBackground {
//            (objects: [PFObject]?, error: Error?) -> Void in
//            if error == nil {
//                self.carObject = objects as! [CarDatabase]
//                self.carDetails.carName = (self.carObject.first?.carName)!
//                self.carDetails.vin = (self.carObject.first?.vin)!
//                
//                self.carDetails.saveInBackground(block: { (success, error) -> Void in
//                    print("The car has been added to the user's list.")
//                    //Add an alert with the same message.
//                })
//            }
//        }
//    }
}

