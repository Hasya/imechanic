//
//  InputFuelViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 4/16/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit
import Parse

class InputFuelViewController: UIViewController {

    var OdoArray:[Int]=[]
    var TotalPriceArray:[Double]=[]
    var totalPrice:Double = 0.0
    
    @IBOutlet weak var OdometerTF: UITextField!
    
    @IBOutlet weak var numGallons: UITextField!
    
    @IBOutlet weak var GallonsPrice: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
  //MARK:-Actions
    
    @IBAction func Calculate(_ sender: UIButton) {
        
        let num:Double = Double(numGallons.text!)!
        let price:Double =  Double(GallonsPrice.text!)!
        
        totalPrice = num * price
        
        OdoArray.append(Int(OdometerTF.text!)!)
        TotalPriceArray.append(Double(totalPrice))
        
        let userFuelDetails = UserFuelDetails()
        
        userFuelDetails.Odometer = Int(OdometerTF.text!)!
        userFuelDetails.TotalPrice = Double(totalPrice)
        userFuelDetails.userID = String((PFUser.current()?.username!)!)!
        userFuelDetails.saveInBackground()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let dest = segue.destination as! FuelTableViewController
        dest.odoArray = OdoArray
        dest.priceArray = TotalPriceArray
    }
    

}
