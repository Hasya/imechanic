//
//  RegisterViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 2/27/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit
import Parse


class RegisterViewController: UIViewController {
    
    @IBOutlet var emailTF: UITextField!
    
    @IBOutlet var usernameTF: UITextField!
    
    @IBOutlet var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      // let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
      // view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUp(_ sender: AnyObject) {
        
        let username = self.usernameTF.text
        let password = self.passwordTF.text
        let email = self.emailTF.text
        let finalEmail = email!.trimmingCharacters(in:.whitespaces)
        
        
        // Validate the text fields
        if (username?.characters.count)! < 5 {
            let alert = UIAlertController(title: "Invalid!", message:"Username must be greater than 5 characters", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
            self.present(alert, animated: true){}
        } else if (password?.characters.count)! < 8 {
            let alert = UIAlertController(title: "Invalid!", message:"Password must be greater than 8 characters", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
            self.present(alert, animated: true){}
            
        } else if (email?.characters.count)! < 8 {
            let alert = UIAlertController(title: "Invalid!", message:"Please enter a valid email address", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
            self.present(alert, animated: true){}
            
        } else {
            
            // Run a spinner to show a task in progress
            
            let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x:0, y:0, width:150, height:150)) as UIActivityIndicatorView
            spinner.startAnimating()
            
            let newUser = PFUser()
            
            
            newUser.username = username
            newUser.password = password
            newUser.email = finalEmail
                        // Sign up the user asynchronously
            
            newUser.signUpInBackground(block: { (succeed, error) -> Void in
                
                // Stop the spinner
                
                spinner.stopAnimating()
                if ((error) != nil) {
                    let alert = UIAlertController(title: "Error", message:"\(String(describing: error))", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
                    self.present(alert, animated: true){}
                } else {
                    let alert = UIAlertController(title: "Success", message:"Signed up successfully!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
                    self.present(alert, animated: true){}
                    DispatchQueue.main.async(execute: { () -> Void in
                        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login")
                        self.present(viewController, animated: true, completion: nil)
                    })
                }
            })
        }
        
    
    
    // Methods to dismiss the keyboard
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    // Methods to reset the fields
    }
    
    @IBAction func resetFields(_ sender: AnyObject) {
        emailTF.text! = ""
        usernameTF.text! = ""
        passwordTF.text! = ""
    }


    
}


