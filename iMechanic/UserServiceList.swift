//
//  UserServiceList.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 4/16/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import Foundation
import Parse

class UserServiceList:PFObject,PFSubclassing {
    
    @NSManaged var monthsLeft:Int
    @NSManaged var Service:String
    @NSManaged var userID:String
    
    
    static func parseClassName() -> String {
        return "UserServiceList"
    }
}
