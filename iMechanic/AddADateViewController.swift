//
//  AddADateViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 4/14/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit

protocol AddADateViewControllerDelegate : class {
//    func addEventTableViewControllerDidCancel(controller: AddEventTableViewController)
    func addDate(controller: AddADateViewController, didFinishAddingDate date: Date)
    
}

class AddADateViewController: UIViewController {
   
    var registrationMonthsLeft:String = ""
    var tyreRotationMonthsLeft:String = ""
    var oilChangeMonthsLeft:String = ""
    var safetyInspectionMonthsLeft:String = ""
    
    var selectedRow:Int!

    var numOfMonthsLeft:String!
    
    
    weak var delegate:AddADateViewControllerDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var DateEntered: UIDatePicker!

    @IBAction func AddDate(_ sender: Any) {
        
        self.delegate.addDate(controller: self, didFinishAddingDate: DateEntered.date)
        
//        let formatter = DateFormatter()
//        formatter.calendar = DateEntered.calendar
//         formatter.dateStyle = .medium
//          formatter.timeStyle = .none
////        let dateString = formatter.string(from: DateEntered.date)
//        
//        
//        let numOfDays = DateEntered.date.daysBetweenDate(toDate: NSDate() as Date)
//        print(numOfDays)
     }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
