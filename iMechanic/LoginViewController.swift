//
//  LoginViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 2/27/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit
import Parse
import Bolts

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var usernameTF: UITextField!
    
    @IBOutlet var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func login(_ sender: AnyObject) {
        
        let username = self.usernameTF.text
        let password = self.passwordTF.text
        
        // Validate the text fields
        
        if (username?.characters.count)! < 5 {
            let alert = UIAlertController(title: "Invalid!", message:"Username and password do not match", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
            self.present(alert, animated: true){}
        } else if (password?.characters.count)! < 5 {
            let alert = UIAlertController(title: "Invalid!", message:"Password must be greater than 8 characters", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
            self.present(alert, animated: true){}
            
        } else {
            
            // Run a spinner to show a task in progress
            
            let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x:0,y:0, width:150, height:150)) as UIActivityIndicatorView
            spinner.startAnimating()
            
            // Send a request to login
            
            PFUser.logInWithUsername(inBackground: username!, password: password!, block: { (user, error) -> Void in
                
                // Stop the spinner
                
                spinner.stopAnimating()
                
                if ((user) != nil) {
                    let alert = UIAlertController(title: "Success", message:"Logged In!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
                        DispatchQueue.main.async(execute: { () -> Void in
//                            self.performSegue(withIdentifier: "carDetailsSegue", sender: nil)
                            let viewController:UIViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "RootNavigationController")
                            self.present(viewController, animated: true, completion: nil)
                        })
                    })
                    self.present(alert, animated: true){}
                    
                } else {
                    let alert = UIAlertController(title: "Error!", message:"Username and password do not match. Please check your credentials.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
                    self.present(alert, animated: true){}
                }
            })
        }
    }
    
    // Methods to dismiss the keyboard
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    // Method to reset the fields
    
    @IBAction func reset(_ sender: AnyObject) {
        usernameTF.text = ""
        passwordTF.text = ""
    }
    
    
    // Unwind segue method to login screen
    
    @IBAction func unwindToLogInScreen(_ segue:UIStoryboardSegue) {
    }
}
