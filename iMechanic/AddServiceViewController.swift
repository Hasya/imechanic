//
//  AddServiceViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 4/14/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit

protocol AddServiceViewControllerDelegate : class {
    //    func addEventTableViewControllerDidCancel(controller: AddEventTableViewController)
    func addDate(controller: AddServiceViewController, didFinishAddingDate date: Date)
}

class AddServiceViewController: UIViewController {
   
    var numOfMonthsLeft:String!
    var serviceList:[String]=[]
    var monthsLeft:String!
    
    
    weak var delegate:AddServiceViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var dateSelected: UIDatePicker!
    
    @IBOutlet weak var Service: UITextField!
    
    // MARK:Actions
    
    @IBAction func AddService(_ sender: UIButton) {
        
      self.delegate.addDate(controller: self, didFinishAddingDate: dateSelected.date)
        
        serviceList.append(Service.text!)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        
//        let dest = segue.destination as! ServiceHistoryTableViewController
//        dest.type = serviceList
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
//    }
    

}
