//
//  FuelTableViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 4/16/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit

class FuelTableViewController: UITableViewController {

    var priceArray:[Double]=[]
    var odoArray:[Int]=[]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return odoArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Fuel", for: indexPath)
        
        cell.textLabel?.text! = String(odoArray[indexPath.row])
        cell.detailTextLabel?.text! = String("Total price is \(priceArray[indexPath.row])")

        return cell
    }
    
    @IBAction func BackButton(_ sender: UIBarButtonItem) {
        self.navigationController?.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func unwindSegueForFuelProze(segue: UIStoryboardSegue) {
    
//        let FuelCost = segue.source as! InputFuelViewController
        self.tableView.reloadData()
        
    }
    
    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let inputFuelVC = segue.destination as! InputFuelViewController
        
        inputFuelVC.OdoArray = odoArray
        inputFuelVC.TotalPriceArray = priceArray
    }
 

}
