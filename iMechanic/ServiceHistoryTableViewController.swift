//
//  ServiceHistoryTableViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 4/14/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit
import Parse

class ServiceHistoryTableViewController: UITableViewController {
    
    var userServices:UserServiceList!
    var objAddServiceViewController:AddServiceViewController!
    var carDetails:UserCarDetails!
    var dashboardTBC:DashboardTabBarController!
    var monthsLeft:String = ""
    var type:[String]=[]
    
    override func viewDidLoad() {
        self.tableView.reloadData()
        super.viewDidLoad()

       }
   
    override func viewWillAppear(_ animated: Bool) {
        dashboardTBC = self.tabBarController as! DashboardTabBarController
        carDetails = dashboardTBC.carDetails
        self.navigationItem.title = carDetails.carName
    }
    
    
    @IBAction func BackButton(_ sender: UIBarButtonItem) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return type.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Service", for: indexPath)
        cell.textLabel?.text = type[indexPath.row]
        cell.detailTextLabel?.text = self.monthsLeft != "" ? "\(self.monthsLeft) months left" : ""
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addService" {
            let addServiceVC = segue.destination as! AddServiceViewController
            
            addServiceVC.delegate = self
        }
    }

    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func unwindSegueForAddDate(segue: UIStoryboardSegue) {
        let addServiceVC = segue.source as! AddServiceViewController
        self.monthsLeft = addServiceVC.numOfMonthsLeft
        self.tableView.reloadData()
    }
}

    extension ServiceHistoryTableViewController: AddServiceViewControllerDelegate {
        
        func addDate(controller: AddServiceViewController, didFinishAddingDate date: Date) {
            let numOfMonthsLeft = date.daysBetweenDate(toDate: NSDate() as Date)
            controller.numOfMonthsLeft = String(numOfMonthsLeft * -1)
            controller.performSegue(withIdentifier: "unwindAddService", sender: nil)
    }
}

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */



