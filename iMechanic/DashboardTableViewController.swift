//
//  DashboardTableViewController.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 3/6/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import UIKit
import Parse

class DashboardTableViewController: UITableViewController {
    

    var objAddADateViewController:AddADateViewController!
    var carDetails:UserCarDetails!
    var dashboardTBC:DashboardTabBarController!
    var monthsLeft:String = ""
    var registrationMonthsLeft:String = ""
    var tyreRotationMonthsLeft:String = ""
    var oilChangeMonthsLeft:String = ""
    var safetyInspectionMonthsLeft:String = ""
    
    var type1 = ["Tire Rotation","Registration","Oil Change","Safety Inspection"]
    var images1 = [#imageLiteral(resourceName: "Tire rotation'"),#imageLiteral(resourceName: "Registration"),#imageLiteral(resourceName: "Oil change"),#imageLiteral(resourceName: "Safety inspection")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()
//        fetchData()
       

//        let globalDetails = (UIApplication.shared.delegate as! AppDelegate).globalCar
//        carDetails = globalDetails
    }

    override func viewWillAppear(_ animated: Bool) {
        dashboardTBC = self.tabBarController as! DashboardTabBarController
        carDetails = dashboardTBC.carDetails
        carDetails.registrationDate = self.registrationMonthsLeft
        carDetails.oilChangeDate = self.oilChangeMonthsLeft
        carDetails.safetyInspectionDate = self.safetyInspectionMonthsLeft
        carDetails.tireRotationDate = self.tyreRotationMonthsLeft
        
        self.navigationItem.title = carDetails.carName
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func doneAction(_ sender: UIBarButtonItem) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return type1.count
    }

   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dash", for: indexPath)

        cell.textLabel?.text = type1[indexPath.row]
        cell.imageView?.image = images1[indexPath.row]
    if indexPath.row == 0 {
        cell.detailTextLabel?.text = self.tyreRotationMonthsLeft != "" ? "\(self.tyreRotationMonthsLeft) months left" : ""
    }
    else if indexPath.row == 1 {
        cell.detailTextLabel?.text = self.registrationMonthsLeft != "" ? "\(self.registrationMonthsLeft) months left" : ""
    } else if indexPath.row == 2 {
        cell.detailTextLabel?.text = self.oilChangeMonthsLeft != "" ? "\(self.oilChangeMonthsLeft) months left" : ""
    } else if indexPath.row == 3 {
        cell.detailTextLabel?.text = self.safetyInspectionMonthsLeft != "" ? "\(self.safetyInspectionMonthsLeft) months left" : ""
    }
    
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        switch indexPath.row{
        case 0:
            tyreRotationMonthsLeft = String(0)
        case 1:
            registrationMonthsLeft = String(0)
        case 2:
            oilChangeMonthsLeft = String(0)
        case 3:
            safetyInspectionMonthsLeft = String(0)
       default: break
            
        }
    }

    
    // MARK: - Navigation

    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func unwindSegueForAddDate(segue: UIStoryboardSegue) {
        let addDateVC = segue.source as! AddADateViewController
        
        self.registrationMonthsLeft = addDateVC.registrationMonthsLeft
        self.oilChangeMonthsLeft = addDateVC.oilChangeMonthsLeft
        self.safetyInspectionMonthsLeft = addDateVC.safetyInspectionMonthsLeft
        self.tyreRotationMonthsLeft = addDateVC.tyreRotationMonthsLeft
        
        let someQuery = PFQuery(className: "UserCarDetails")
        someQuery.getObjectInBackground(withId: carDetails.objectId!) {
            (updatedObject: PFObject?, error: Error?) -> Void in
            if error != nil {
                print(error)
            } else if let updatedObject = updatedObject {
                updatedObject["registrationDate"] = self.registrationMonthsLeft
                updatedObject["tireRotationDate"] = self.tyreRotationMonthsLeft
                updatedObject["oilChangeDate"] = self.oilChangeMonthsLeft
                updatedObject["safetyInspectionDate"] = self.safetyInspectionMonthsLeft
                
                updatedObject.saveInBackground()
            }
            
        }
        
//        self.carDetails.saveInBackground(block: { (success, error) -> Void in
//            print("The car has been added to the user's list.")
//            //Add an alert with the same message.
//            
//            self.navigationController?.popViewController(animated: true)
//        })
        
        print(addDateVC.numOfMonthsLeft)
        
        self.tableView.reloadData()
       }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addDate" {
    
            let addDateVC = segue.destination as! AddADateViewController
        
            addDateVC.selectedRow = self.tableView.indexPathForSelectedRow?.row
            addDateVC.oilChangeMonthsLeft = oilChangeMonthsLeft
            addDateVC.registrationMonthsLeft = registrationMonthsLeft
            addDateVC.safetyInspectionMonthsLeft = safetyInspectionMonthsLeft
            addDateVC.tyreRotationMonthsLeft = tyreRotationMonthsLeft
            addDateVC.delegate = self
        }
    }
}

extension DashboardTableViewController : AddADateViewControllerDelegate {
    
    func addDate(controller: AddADateViewController, didFinishAddingDate date: Date) {
        let numOfMonthsLeft = date.daysBetweenDate(toDate: NSDate() as Date)
        switch Int((self.tableView.indexPathForSelectedRow?.row)!) {
        case 0: controller.tyreRotationMonthsLeft = String(numOfMonthsLeft * -1)
                print(numOfMonthsLeft)
        case 1: controller.registrationMonthsLeft = String(numOfMonthsLeft * -1)
                print(numOfMonthsLeft)
        case 2: controller.oilChangeMonthsLeft = String(numOfMonthsLeft * -1)
                print(numOfMonthsLeft)
        case 3: controller.safetyInspectionMonthsLeft = String(numOfMonthsLeft * -1)
                print(numOfMonthsLeft)
        default: print("No row was selected")
        }
        
        controller.performSegue(withIdentifier: "unwindAddDate", sender: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
//    func fetchData(){
//        let query = PFQuery(className:"UserCarDetails")
//        query.whereKey("userID", equalTo: (PFUser.current()?.username!)!)
//        query.findObjectsInBackground {
//            (objects: [PFObject]?, error: Error?) -> Void in
//            if error == nil {
//                self.carDetails = objects as! [UserCarDetails]
//                self.registrationMonthsLeft = (self.carDetails.registrationDate)
//                
//                self.tableView.reloadData()
//            }
//        }
//    }
  
    
}
