//
//  UserFuelDetails.swift
//  iMechanic
//
//  Created by Sandeep Velaga on 4/16/17.
//  Copyright © 2017 hasya. All rights reserved.
//

import Foundation
import Parse

class UserFuelDetails:PFObject,PFSubclassing {
    
    @NSManaged var Odometer:Int
    @NSManaged var TotalPrice:Double
    @NSManaged var userID:String
    
    static func parseClassName() -> String {
        return "UserFuelDetails"
    }
}
